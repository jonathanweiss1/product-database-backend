package net.planic.productdatabasebackend.db.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity(name="product")
public class Product {

    // Constructors
    protected Product () {}

    public Product (long articleNo, String name, float price) {
        this.articleNo = articleNo;
        this.name = name;
        this.price = price;
    }

    // variables
    @Id
    @Column(unique = true)
    private Long articleNo;
    private String name;
    private float price;

    // methods
    @Override
    public String toString() {
        return String.format(
                "Product[articleNo=%d, name=%s, price=%f]",
                articleNo, name, price
        );
    }

    public Long getArticleNo() {
        return articleNo;
    }

    public void setArticleNo(Long articleNo) {
        this.articleNo = articleNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
